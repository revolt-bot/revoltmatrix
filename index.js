const { MatrixClient,
    SimpleFsStorageProvider,
    AutojoinRoomsMixin,} = require("matrix-bot-sdk")

const configs= require("./config.json");
const storage = new SimpleFsStorageProvider("hello-bot.json");
const revolt = require("revolt.js");
const rev_client = new revolt.Client();
const fs = require("fs");
const mat_client = new MatrixClient(configs.matrix_domain, configs.matrix_token, storage);
var room_list= require("./room_list.json");
AutojoinRoomsMixin.setupOnClient(mat_client);

process.on("uncaughtException", (err) => {
    console.error(`Uncaught error:\n${err.stack}`);
	console.log(err);
});

function clean_list(){
	/* WIP

	id=null;
	list=room_list.matrix;
	list2=room_list.revolt;
	
	
	data=JSON.stringify(room_list);
	console.log(data);
	fs.writeFileSync('./room_list.json', data);
	/*
}


function browse_list(id){//return both room ids
	let list;
	let index;

	if(id.includes(":"))
		list=room_list.matrix;
	else
		list=room_list.revolt;
	
	index=list.lastIndexOf(id);
	if(index<0)
			return -1;//the id is not on the list. -1 is error
return [room_list.matrix[index], room_list.revolt[index]];
}

class BRIDGE_CREDENTIIALS {
	constructor(id) {
		  this.id =id;
		  this.pw= Number(new Date())*86472*Math.random();
	  }
	register(id){
		  this.id =id;
		  this.pw= Number(new Date())*86472*Math.random();
		  }
	login(other_id,pw){
		  if(pw==this.pw){
			  this.other_id=other_id;
			  return 1;
			  }

		  else
			  return 0;
		  }
	swap(){
		  let buffer="a"+this.id;
		  this.id="a"+this.other_id;
		  this.id=this.id.substr(1);
		  this.other_id=buffer.substr(1);
		  
		  }
	update_list(){
		let length=room_list.matrix.length;
		room_list.matrix[length]=this.other_id
		room_list.revolt[length]=this.id;
		let data=JSON.stringify(room_list);
		fs.writeFileSync('./room_list.json', data);
	  }
}   //Bridge credentials, I will let .id =revolt channel ID, .other_id=matrix channel id


var credentials = new BRIDGE_CREDENTIIALS();

clean_list();
var channel_list = [] // a list of channels, [Rev mat]



mat_client.start().then(() => console.log("Matrix Bot started!"));



rev_client.on("ready", () => {
	console.log(`Revolt client logged ${rev_client.user.username}`);
});

rev_client.loginBot(configs.revolt_bot_token); //revolt login




async function Send_image_matrix(url, roomId){
	//upload url
	// Send event
	mat_client.uploadContentFromUrl(url).then(mxcurl => {
		let imageJSON = {
		  msgtype: 'm.image',
		  body: 'ebend.png',
		  url: mxcurl
		};
		
		mat_client.sendRawEvent(roomId, 'm.room.message', imageJSON);
		});

}


async function sendTextMessage(bodyy, ismatrix,roomId,rev_channel){
	if(ismatrix){
		
		formated = bodyy.split("**")
		if(formated.length>1){
			formated[1]="<strong>"+formated[1]+"</strong><br>"
			}
			
		mat_client.sendEvent(roomId, "m.room.message", { //Send MAtrix message	
		"body": bodyy,
		"format": "org.matrix.custom.html",
		"formatted_body": formated.join(""),
		"msgtype": "m.text"}, 
		"").then((res) => {}).catch((err) => {	console.log(err); })
	}
	else if(!ismatrix){

		rev_channel.sendMessage(bodyy);
	}
}





async function handle_commands(args,channel_id, Pointer,ismatrix){
	switch (args[0]){
		case "$$login" :
			let password = args[1];
			
			let attempt=credentials.login(channel_id,password)
			sendTextMessage("Login  Executed",ismatrix,channel_id,Pointer);

			if(!ismatrix && attempt)
				credentials.swap();
			if(attempt)
				credentials.update_list();
			else
				sendTextMessage("Failed, Wrong password",ismatrix,channel_id,Pointer);

			break;
			
		case "$$register" :
			credentials.register(channel_id);
			sendTextMessage("Registered, the password is:  $$login "+credentials.pw , ismatrix, channel_id, Pointer);
			break;

		case "$$m":
			credentials.register(channel_id);
			args[0]="";
			channel_id_m=browse_list(channel_id);
			rev_channel=rev_client.channels.get(channel_id_m[1]);
			rev_channel.sendMessage({
						content: args.join(" "),
						masquerade: {
							name: "A moderator",
							avatar: "", //link to the avatar
						}
			})
			break;
	}
	
	}




//room_list.matrix.length
rev_client.on("messageCreate", (message) => { //Revolt message event
	if(message.channel.channel_type=="Group"){ 
		return;
		}

	if(message.author.id == "01H3B5BRQVJ5644Z2KDSKF0WF1"){return;} //inores bot messages

	let args = message.content.split(" ")
	let bridge=browse_list(message.channelId);	
	


	if(bridge[0]){

		matrix_message="**"+message.author.username+":** \n>"+message.content;
		
		if(message.masquerade!=null)
			matrix_message="masqueraded bot "+message.masquerade.name+":\n> "+message.content;
	
		att=message.attachments;

		if(att!=null){
			imag=att[0];
			id=imag.id;
			fname=imag.filename;
			p1 = "https://autumn.revolt.chat/attachments/"
			url=p1+id+"/"+fname;

			Send_image_matrix(url,bridge[0]).catch();
		}
			

		sendTextMessage(matrix_message,1, bridge[0], message.channel);
	}
	
	
	if(message.member.hasPermission(message.channel,"ManageServer"))
		handle_commands(args,message.channel.id,message.channel,0);
	
		

});





mat_client.on("room.message", (roomId, event) => {//Matrix message event
    // `event['type']` will always be an `m.room.message` and can be processed as such
    // be sure to check if the event is redacted/invalid though:
    if (!event['content']?.['msgtype']) return;
	if (!event['sender']) return;
	

	let body = event['content']['body'];
	args=body.split(" ");
	bridge=browse_list(roomId);
	let Isadmin=mat_client.userHasPowerLevelFor(event['sender'],roomId,"m.room.power_levels",1).then(r => {
		//console.log("handle commands_matrix");	
		handle_commands(args,roomId,null,1);
		});
	
	if (event['sender'] === configs.matrix_login) return;
	let sender=event['sender'].split(":");
	
	//console.log("user is admin" + Isadmin);
	if(bridge[0]){
		if(event['content']?.['msgtype']=="m.image"){
			filtered_url=event.content.url.substr(6);
			filtered_url=configs.matrix_domain+"/_matrix/media/r0/download/"+filtered_url;
			body="[]("+filtered_url+")";
		
		}
			
		mat_client.getUserProfile(event['sender']).then((profile)=>{
			
			if(!profile.avatar_url)
				profile.avatar_url="https://matrix.org/_matrix/media/r0/download/matrix.org/DLkrLeCwYLKJCGFFINpaNGJv";
			
			filtered_url=profile.avatar_url.substr(6).split("/");
			filtered_url=configs.matrix_domain+"/_matrix/media/r0/download/"+sender[1]+"/"+filtered_url[1];

			rev_channel=rev_client.channels.get(bridge[1]);
			rev_channel.sendMessage({
					content: body,
					//replies: reply ? [{ id: reply.Rid, mention: false }] : undefined,
					masquerade: {
						name: sender[0],
						avatar: filtered_url,
					}
				})
			}).catch((err) => {	console.log(err)});
	}
	

	
	//console.log(event);

});


var edit_timeout=0;

rev_client.on("message/update", async (message,previousMessage) => {
	if(message.author.id == "01H3B5BRQVJ5644Z2KDSKF0WF1") return; //inores bot messages
	let args = message.content.split(" ")
	let bridge=browse_list(message.channel.id);	
	
	if(message.author.id == "01H3B5BRQVJ5644Z2KDSKF0WF1") return; 	
	
	
	if (Date.now()<edit_timeout+20000)
		return;
	edit_timeout= Date.now();

	
	if(bridge[0]){
		//console.log(message.attachments);
		matrix_message=message.author.username+":\n> "+message.content;
		
		if(message.masquerade!=null)
			matrix_message="masqueraded bot "+message.masquerade.name+":\n> "+message.content;
	
		sendTextMessage(">"+matrix_message,1	, bridge[0], message.channel);


	}
})





