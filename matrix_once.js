const configs = require("./config.json")
var myUserId = configs.matrix_login;
var myAccessToken = configs.matrix_password;
const { MatrixClient} = require("matrix-bot-sdk")
require("MatrixAuth");
// This will be the URL where clients can reach your homeserver. Note that this might be different
// from where the web/chat interface is hosted. The server must support password registration without
// captcha or terms of service (public servers typically won't work).
const homeserverUrl = configs.matrix_domain;

const auth = new MatrixAuth(homeserverUrl);
const client = await auth.passwordLogin(configs.matrix_login, configs.matrix_password);

console.log("Copy this access token to your bot's config: ", client.accessToken);
